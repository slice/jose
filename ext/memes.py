import random
import string
import urllib.parse
import asyncio
import re

from io import BytesIO
from urllib.parse import quote_plus
from random import randrange

import discord
import aiohttp

from discord.ext import commands
from .common import Cog, WIDE_MAP

MAX_MEME_NAME = 30

RI_TABLE = {
    "0": ":zero:",
    "1": ":one:",
    "2": ":two:",
    "3": ":three:",
    "4": ":four:",
    "5": ":five:",
    "6": ":six:",
    "7": ":seven:",
    "8": ":eight:",
    "9": ":nine:",
    ".": ":record_button:",
    "!": ":exclamation:",
    "?": ":question:",
    "+": ":heavy_plus_sign:",
    "-": ":heavy_minus_sign:",
}

# implement letters
RI_STR = "🇦🇧🇨🇩🇪🇫🇬🇭🇮🇯🇰🇱🇲🇳🇴🇵🇶🇷🇸🇹🇺🇻🇼🇽🇾🇿"
RI_TABLE.update(
    {
        letter: RI_STR[string.ascii_lowercase.find(letter)]
        for letter in string.ascii_lowercase
    }
)


class Memes(Cog):
    def __init__(self, bot):
        super().__init__(bot)
        self.urban_cache = {}

    @commands.command(aliases=["fw"])
    async def fullwidth(self, ctx, *, text: str):
        """Convert text to full width."""
        if len(text.strip()) <= 0:
            return

        await ctx.send(text.translate(WIDE_MAP))

    @commands.command()
    async def ri(self, ctx, *, inputstr: str):
        """Convert text to Regional Indicators."""
        inputstr = inputstr.strip().lower()
        l_inputstr = len(inputstr)
        if l_inputstr < 1 or l_inputstr > 1995:
            await ctx.send("lul")
            return

        inputstr = list(inputstr)

        for (index, char) in enumerate(inputstr):
            if char in RI_TABLE:
                inputstr[index] = f"{RI_TABLE[char]}\u200b"

        res = "".join(inputstr)
        await ctx.send(res)

    @commands.command(hidden=True)
    async def pupper(self, ctx):
        await ctx.send("http://i.imgur.com/9Le8rW7.jpg :sob:")

    @commands.command(name="8ball")
    async def _8ball(self, ctx):
        """8ball lul"""
        answer = random.choice(
            ["Yes", "No", "Maybe", "Potentially", "Only in your dreams"]
        )

        await ctx.send(f"**{ctx.author.name}**, :8ball: said {answer}.")

    @commands.command(hidden=True)
    async def br(self, ctx):
        """br?"""
        await ctx.send("br?")

    @commands.command()
    async def urban(self, ctx, *, term: str):
        """Search for a word in the Urban Dictionary."""

        # cache :DDDDDDDDD
        if term in self.urban_cache:
            definition = self.urban_cache[term][0]
            example = self.urban_cache[term][1]
            return await ctx.send(f"```\n{term!r}:\n" f"{definition}\n{example}```")

        await self.jcoin.pricing(ctx, self.prices["API"])

        termq = urllib.parse.quote(term)
        urban_url = f"https://api.urbandictionary.com/v0/define?term={termq}"
        content = await self.get_json(urban_url)
        c_list = content["list"]

        if len(c_list) < 1:
            raise self.SayException("No results found")

        definition = c_list[0]["definition"]
        example = ""
        if len(definition) < 1940:
            example = c_list[0]["example"]

        self.urban_cache[term] = [definition, example]
        await ctx.send(f"```\n{term!r}:\n{definition}\n{example}```")

    @commands.command(hidden=True)
    async def ejaculate(self, ctx):
        """PUMPE THE MOOSCLES YIIIS"""
        await ctx.send("https://www.youtube.com/watch?v=S6UqgjaBt4w")

    @commands.command()
    @commands.is_owner()
    async def blink(self, ctx, *, text: str):
        m = await ctx.send(text)
        for i in range(10):
            if i % 2 == 0:
                await m.edit(content=f"**{text}**")
            else:
                await m.edit(content=f"{text}")
            await asyncio.sleep(2)

    @commands.command()
    @commands.is_nsfw()
    async def neko(self, ctx):
        """Posts a random neko picture."""
        api_url = "http://nekos.life/api/neko"

        response = await self.get_json(api_url)
        image_url = response["neko"]

        embed = discord.Embed(color=discord.Color(0xF84A6E))
        embed.set_image(url=image_url)

        await ctx.send(embed=embed)

    @commands.command()
    async def owo(self, ctx, *, text: commands.clean_content):
        """hewwo!!"""
        replacement_table = {
            r"[rl]": "w",
            r"[RL]": "W",
            r"n([aeiou])": "ny\\1",
            r"N([aeiou])": "Ny\\1",
            r"ove": "uv",
        }

        for regex, replace_with in replacement_table.items():
            text = re.sub(regex, replace_with, text)

        await ctx.send(text)

    @commands.command()
    async def logo(self, ctx, *, sentence: commands.clean_content):
        """logo-ify!"""
        sentence2 = []
        guild = self.bot.get_guild(466088422851870720)
        emoji_name = None

        for letter in sentence:
            if letter == " ":
                sentence2.append(5 * " ")
                continue

            emoji_name = f"Logo{letter.upper()}"

            if letter == ".":
                emoji_name = "LogoPeriod"
            elif letter == "!":
                emoji_name = "LogoExclam"

            def _check(emoji):
                return emoji.name == emoji_name

            emoji = discord.utils.find(_check, guild.emojis)

            if not emoji:
                continue

            sentence2.append(str(emoji))

        res = "".join(sentence2)
        if not res:
            return await ctx.send("no")

        try:
            await ctx.send(res)
        except discord.HTTPException:
            await ctx.send(
                "The resulting logofied message was too large. \n"
                "This is caused by how discord represents emotes "
                "internally."
            )

    @commands.command()
    async def lesbian(self, ctx):
        """are you led bian??"""
        await ctx.send("https://www.youtube.com/watch?v=j3kyeOUrdS4")

    @commands.command()
    @commands.cooldown(1, 2, commands.BucketType.user)
    async def orly(self, ctx, title, guide, author, *, top_text=""):
        """
        Generates O'Reilly book covers.

        Original code from dogbot. (https://github.com/slice/dogbot).
        """

        # Modified to follow josé's general style guide.

        api_base = "https://orly-appstore.herokuapp.com/generate?"

        url = api_base + (
            f"title={quote_plus(title)}&top_text="
            f"{quote_plus(top_text)}&image_code="
            f"{randrange(0, 41)}&theme={randrange(0, 17)}"
            f"&author={quote_plus(author)}&guide_text="
            f"{quote_plus(guide)}"
            "&guide_text_placement=bottom_right"
        )

        try:
            async with ctx.typing():
                bio = await self.http_read(url)
                await ctx.send(file=discord.File(filename="orly.png", fp=bio))
        except aiohttp.ClientError:
            await ctx.send("Couldn't contact the API.")

    @commands.command()
    async def love(self, ctx, subject1: str, subject2: str):
        """Give love statistics about any two subjects.

        (n-subjects is too much intensive. sorry poly people,
        your love is too much)
        """
        random.seed(subject1 + subject2)
        love_12 = random.randint(0, 100)
        random.seed(subject2 + subject1)
        love_21 = random.randint(0, 100)

        # reseed with system time to not fuck things up down the line
        random.seed()

        em = discord.Embed(title="love stats", description="")
        em.description += f"{subject1} => {subject2} is {love_12}% love\n"
        em.description += f"{subject2} => {subject1} is {love_21}% love"

        await ctx.send(embed=em)


def setup(bot):
    bot.add_cog(Memes(bot))
    bot.remove_command("meme")
